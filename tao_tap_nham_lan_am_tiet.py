# -*- coding: utf-8 -*-

from __future__ import print_function
import re
import sys


# =============================== xoa dau ========================================
patterns = {
    'á': 'a',
    'à': 'a',
    'ả': 'a',
    'ạ': 'a',
    'ã': 'a',
    'ắ': 'ă',
    'ằ': 'ă',
    'ẳ': 'ă',
    'ẵ': 'ă',
    'ặ': 'ă',
    'ấ': 'â',
    'ầ': 'â',
    'ẩ': 'â',
    'ẫ': 'â',
    'ậ': 'â',
    'é': 'e',
    'è': 'e',
    'ẻ': 'e',
    'ẽ': 'e',
    'ẹ': 'e',
    'ế': 'ê',
    'ề': 'ê',
    'ể': 'ê',
    'ễ': 'ê',
    'ệ': 'ê',
    'í': 'i',
    'ì': 'i',
    'ỉ': 'i',
    'ĩ': 'i',
    'ị': 'i',
    'ó': 'o',
    'ò': 'o',
    'ỏ': 'o',
    'õ': 'o',
    'ọ': 'o',
    'ố': 'ô',
    'ồ': 'ô',
    'ổ': 'ô',
    'ỗ': 'ô',
    'ộ': 'ô',
    'ớ': 'ơ',
    'ờ': 'ơ',
    'ở': 'ơ',
    'ỡ': 'ơ',
    'ợ': 'ơ',
    'ú': 'u',
    'ù': 'u',
    'ủ': 'u',
    'ũ': 'u',
    'ụ': 'u',
    'ứ': 'ư',
    'ừ': 'ư',
    'ử': 'ư',
    'ữ': 'ư',
    'ự': 'ư',
    'ý': 'y',
    'ỳ': 'y',
    'ỷ': 'y',
    'ỹ': 'y',
    'ỵ': 'y'
}
def convert(text):
    output = text
    for regex, replace in patterns.items():
        output = re.sub(regex, replace, output)
        # deal with upper case
        # output = re.sub(regex.upper(), replace.upper(), output)
    return output
# =============================== ket thuc xoa dau ==================================

# ================================== them dau =======================================
patterns_them_dau = [
    ['a', 'á', 'à', 'ả', 'ã', 'ạ'],
	['ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ'],
	['â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ'],
	['e', 'é', 'è', 'ẻ', 'ẽ', 'ẹ'],
	['ê', 'ế', 'ề', 'ể', 'ễ', 'ệ'],
	['i', 'í', 'ì', 'ỉ', 'ĩ', 'ị'],
	['o', 'ó', 'ò', 'ỏ', 'õ', 'ọ'],
	['ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ'],
	['ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ'],
	['u', 'ú', 'ù', 'ủ', 'ũ', 'ụ'],
	['ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự'],
	['y', 'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ'],
	['ai', 'ái', 'ài', 'ải', 'ãi', 'ại'],
	['ao', 'áo', 'ào', 'ảo', 'ão', 'ạo'],
	['au', 'áu', 'àu', 'ảu', 'ãu', 'ạu'],
	['ay', 'áy', 'ày', 'ảy', 'ãy', 'ạy'],
	['âu', 'ấu', 'ầu', 'ẩu', 'ẫu', 'ậu'],
	['ây', 'ấy', 'ầy', 'ẩy', 'ẫy', 'ậy'],
	['eo', 'éo', 'èo', 'ẻo', 'ẽo', 'ẹo'],
	['êu', 'ếu', 'ều', 'ểu', 'ễu', 'ệu'],
	['ia', 'ía', 'ìa', 'ỉa', 'ĩa', 'ịa'],
	['iu', 'íu', 'ìu', 'ỉu', 'ĩu', 'ịu'],
	['iê', 'iế', 'iề', 'iể', 'iễ', 'iệ'],
	['oa', 'óa', 'òa', 'ỏa', 'õa', 'ọa'],
	['oă', 'oắ', 'oằ', 'oẳ', 'oẵ', 'oặ'],
	['oe', 'óe', 'òe', 'ỏe', 'õe', 'ọe'],
	['oi', 'ói', 'òi', 'ỏi', 'õi', 'ọi'],
	['oo'],
	['ôi', 'ối', 'ồi', 'ổi', 'ỗi', 'ội'],
	['ơi', 'ới', 'ời', 'ởi', 'ỡi', 'ợi'],
	['ua', 'úa', 'ùa', 'ủa', 'ũa', 'ụa'],
	['uâ', 'uấ', 'uầ', 'uẩ', 'uẫ', 'uậ'],
	['ui', 'úi', 'ùi', 'ủi', 'ũi', 'ụi'],
	['uê', 'uế', 'uề', 'uể', 'uễ', 'uệ'],
	['uô', 'uố', 'uồ', 'uổ', 'uỗ', 'uộ'],
	['uơ', 'uớ', 'uờ', 'uở', 'uỡ', 'uợ'],
	['uy', 'úy', 'ùy', 'ủy', 'ũy', 'ụy'],
	['ưa', 'ứa', 'ừa', 'ửa', 'ữa', 'ựa'],
	['ưi', 'ứi', 'ừi', 'ửi', 'ữi', 'ựi'],
	['ươ', 'ướ', 'ườ', 'ưở', 'ưỡ', 'ượ'],
	['ưu', 'ứu', 'ừu', 'ửu', 'ữu', 'ựu'],
	['yê', 'yế', 'yề', 'yể', 'yễ', 'yệ'],
	['oai', 'oái', 'oài', 'oải', 'oãi', 'oại'],
	['oay', 'oáy', 'oày', 'oảy', 'oãy', 'oạy'],
	['uây', 'uấy', 'uầy', 'uẩy', 'uẫy', 'uậy'],
	['uôi', 'uối', 'uồi', 'uổi', 'uỗi', 'uội'],
	['iêu', 'iếu', 'iều', 'iểu', 'iễu', 'iệu'],
	['uyê', 'uyế', 'uyề', 'uyể', 'uyễ', 'uyệ'],
	['ươu', 'ướu', 'ườu', 'ưởu', 'ưỡu', 'ượu'],
	['ươi', 'ưới', 'ười', 'ưởi', 'ưỡi', 'ượi'],
	['uya', 'uýa', 'uỳa', 'uỷa', 'uỹa', 'uỵa'],
	['yêu', 'yếu', 'yều', 'yểu', 'yễu', 'yệu'],
	['uyu', 'uýa', 'uỳa', 'uỷa', 'uỹa', 'uỵa']
]

def them_dau(text):
	for i in xrange(len(patterns_them_dau)):
		if patterns_them_dau[i][0] == text:
			return patterns_them_dau[i]
# ================================== ket thuc them dau =====================================

phu_am_dau = [
				['b', 'p', 'v', 'n'],
				['c', 'ch', 'x', 'v'],
				['d', 'đ', 'r', 'gi']
			]
nguyen_am = [
				['a', 'ă', 'â', 'ai', 'ao', 'au', 'ay', 'ia', 'oa', 'ua', 'ưa'],
				['ă', 'a', 'â', 'oă'],
				['â', 'a', 'ă', 'âu', 'ây'],
				['e', 'ê', 'eo', 'oe'],
				['ê', 'e', 'iê', 'yê', 'uê', 'êu'],
				['i', 'u', 'o', 'y', 'ai', 'ia', 'iê', 'oi', 'ôi', 'ơi', 'ui', 'ưi'],
				['o', 'u', 'i', 'ô', 'ơ', 'ao', 'eo', 'oa', 'oă','oe', 'oi', 'oo'],
				['ô', 'o', 'ơ', 'i', 'u', 'ôi', 'uô'],
				['ơ', 'o', 'ô', 'i', 'ư', 'ơi', 'uơ', 'ươ'],
				['u', 'y', 'ư', 'i', 'o', 'au', 'êu', 'iu', 'ua', 'uâ', 'ui', 'uê', 'uô', 'uơ', 'uy'],
				['ư', 'u', 'i', 'ơ', 'ưa', 'ưi', 'ươ', 'ưu'],
				['y', 'u', 'i', 'ay', 'ây', 'uy', 'yê'],
				['ai', 'a', 'i', 'au', 'ay', 'ao', 'oai', 'oay', 'ia', 'oa', 'ua'],
				['ao', 'a', 'o', 'ai', 'au', 'ay', 'oă', 'oa', 'ua', 'ia', 'oai', 'oay'],
				['au', 'a', 'u', 'ai', 'ay', 'ao', 'ưa', 'ia', 'ua', 'oa', 'âu', 'uâ', 'uây', 'uya'],
				['ay'],
				['âu'],
				['ây'],
				['eo'],
				['êu'],
				['ia'],
				['iu'],
				['iê'],
				['oa'],
				['oă'],
				['oe'],
				['oi'],
				['oo'],
				['ôi'],
				['ơi'],
				['ua'],
				['uâ'],
				['ui'],
				['uê'],
				['uô'],
				['uơ'],
				['uy'],
				['ưa'],
				['ưi'],
				['ươ'],
				['ưu'],
				['yê'],
				['oai'],
				['oay'],
				['uây'],
				['uôi'],
				['iêu'],
				['uyê'],
				['ươu'],
				['ươi'],
				['uya'],
				['yêu'],
				['uyu']
			]
phu_am_cuoi = [
				['c', 'ch'],
				['p'],
				['t'],
				['m', 'n'],
				['n', 'ng', 'm'],
				['ch', 'c', 'nh'],
				['ng', 'n', 'nh'],
				['nh', 'ch', 'n', 'ng']
			]

print("Nhap 1 am tiet: ")
am_tiet = raw_input()
am_tiet_khong_dau = convert(am_tiet)

dau = []
na = []
cuoi = []
na_temp = []

ko_pad = ""

for i in xrange(len(phu_am_dau)):
	if am_tiet_khong_dau.startswith(phu_am_dau[i][0]):
		dau = phu_am_dau[i]
		ko_pad = am_tiet_khong_dau[len(phu_am_dau[i][0]):]
		break

for i in xrange(len(phu_am_cuoi)):
	if ko_pad.endswith(phu_am_cuoi[i][0]):
		cuoi = phu_am_cuoi[i]
		ko_pad = ko_pad[:len(ko_pad) - len(phu_am_cuoi[i][0])]
		break

for i in xrange(len(nguyen_am)):
	if ko_pad == nguyen_am[i][0]:
		na_temp = nguyen_am[i]
		break

dau.append("")
cuoi.append("")
for i in xrange(len(na_temp)):
	na.extend(them_dau(na_temp[i]))

print(dau)
print(len(na))
print(cuoi)

kq = [];

for i in xrange(len(dau)):
	for j in xrange(len(na)):
		for k in xrange(len(cuoi)):
			kq.append(dau[i] + na[j] + cuoi[k])

# for x in xrange(len(kq)):
print(len(kq))